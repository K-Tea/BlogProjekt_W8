package de.awacademy.zwitscher;

import de.awacademy.zwitscher.beitrag.BeitragRepository;
import de.awacademy.zwitscher.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

@Controller
public class HomeController {

    private BeitragRepository beitragRepository;

    @Autowired
    public HomeController(BeitragRepository beitragRepository) {
        this.beitragRepository = beitragRepository;
    }

    @GetMapping("/")
    public String home(@ModelAttribute("sessionUser") User sessionUser, Model model) {
        model.addAttribute("beitragList", beitragRepository.findAllByOrderByPostedAtDesc());
        return "home";
    }

}
