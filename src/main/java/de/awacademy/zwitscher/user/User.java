package de.awacademy.zwitscher.user;

import de.awacademy.zwitscher.beitrag.Beitrag;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class
User {

    @Id
    @GeneratedValue
    private long id;

    private String username;
    private String password;
    private boolean isAdmin;

    @OneToMany(mappedBy = "user")
    private List<Beitrag> beitragList;

    public User() {
    }

    public User(String username, String password, boolean isAdmin) {
        this.username = username;
        this.password = password;
        this.isAdmin = isAdmin;

    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public List<Beitrag> getBeitragList() {
        return beitragList;
    }

    public long getId() {
        return id;
    }
}
