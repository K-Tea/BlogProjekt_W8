package de.awacademy.zwitscher.comment;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class CommentDTO {

    @NotBlank
    @Size(min = 1, max = 99)
    @Pattern(regexp = "^[a-zA-Z0-9_.,!? ;'#]+$", message = "Keine seltsamen Sonderzeichen erlaubt!")
    private String text;

    public CommentDTO(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
