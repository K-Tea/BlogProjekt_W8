package de.awacademy.zwitscher.comment;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Repository
@Service
public interface CommentRepository extends CrudRepository<Comment, Long> {
    List<Comment> findAllByOrderByPostedAtDesc();
    List<Comment> findByBeitragId(long id);
}
