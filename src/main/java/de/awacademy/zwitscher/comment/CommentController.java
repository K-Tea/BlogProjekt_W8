package de.awacademy.zwitscher.comment;

import de.awacademy.zwitscher.beitrag.Beitrag;
import de.awacademy.zwitscher.beitrag.BeitragRepository;
import de.awacademy.zwitscher.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.Instant;

@Controller
public class CommentController {

    private CommentRepository commentRepository;
    private BeitragRepository beitragRepository;

    @Autowired
    public CommentController(CommentRepository commentRepository, BeitragRepository beitragRepository) {
        this.commentRepository = commentRepository;
        this.beitragRepository = beitragRepository;
    }

    public CommentController() {
    }


    @GetMapping("/comments/{id}")
    public String beitrag(Model model, @PathVariable("id") long id) {
        Beitrag beitrag = new Beitrag();
        model.addAttribute("commentList", commentRepository.findByBeitragId(id));
        model.addAttribute("comments", commentRepository.findByBeitragId(id));
        model.addAttribute("beitrag", beitragRepository.findById(id).get());
        model.addAttribute("comment" , new de.awacademy.zwitscher.comment.CommentDTO(""));

        return "comments";
    }

    @PostMapping("/comments/{id}")
    public String comment(@Valid @ModelAttribute("comment") CommentDTO commentDTO, BindingResult bindingResult,
                          @ModelAttribute("sessionUser") User sessionUser, @PathVariable long id) {

        if (bindingResult.hasErrors()) {
            return "redirect:/comments/" + id;
        }

        Comment comment = new Comment(sessionUser, commentDTO.getText(), Instant.now(), beitragRepository.findById(id).get());

        commentRepository.save(comment);

        return "redirect:/comments/{id}";
    }


}

