package de.awacademy.zwitscher.comment;

import de.awacademy.zwitscher.beitrag.Beitrag;
import de.awacademy.zwitscher.user.User;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.Instant;

@Entity
public class Comment {

    @Id
    @GeneratedValue
    private long id;

    @ManyToOne
    private User user;

    private String text;
    private Instant postedAt;

    @ManyToOne
    private Beitrag beitrag;


    public Comment(User user, String text, Instant postedAt,Beitrag beitrag) {
        this.user = user;
        this.text = text;
        this.postedAt = postedAt;
        this.beitrag = beitrag;
    }

    public Comment() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Instant getPostedAt() {
        return postedAt;
    }

    public void setPostedAt(Instant postedAt) {
        this.postedAt = postedAt;
    }

    public Beitrag getBeitrag() {
        return beitrag;
    }

    public void setBeitrag(Beitrag beitrag) {
        this.beitrag = beitrag;
    }


}
