package de.awacademy.zwitscher.beitrag;

import de.awacademy.zwitscher.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.time.Instant;

@Controller
public class BeitragController {

    private BeitragRepository beitragRepository;

    @Autowired
    public BeitragController(BeitragRepository beitragRepository) {
        this.beitragRepository = beitragRepository;
    }

    @GetMapping("/beitrag")
    public String beitrag(Model model, @ModelAttribute("sessionUser") User sessionUser) {
        model.addAttribute("beitrag", new BeitragDTO("Dein Text","Dein Titel"));
        return "beitrag";
    }

    @PostMapping("/beitrag")
    public String beitrag(@Valid @ModelAttribute("beitrag") BeitragDTO beitragDTO, BindingResult bindingResult,
                          @ModelAttribute("sessionUser") User sessionUser) {

        if (bindingResult.hasErrors()) {
            return "beitrag";
        }

        Beitrag beitrag = new Beitrag(sessionUser, beitragDTO.getText(), Instant.now(),beitragDTO.getTitle());
        beitragRepository.save(beitrag);

        return "redirect:/";
    }

}
