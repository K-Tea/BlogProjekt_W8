package de.awacademy.zwitscher.beitrag;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BeitragRepository extends CrudRepository<Beitrag, Long> {
    List<Beitrag> findAllByOrderByPostedAtDesc();
}
