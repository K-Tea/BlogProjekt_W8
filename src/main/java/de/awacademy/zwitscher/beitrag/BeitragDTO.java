package de.awacademy.zwitscher.beitrag;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class BeitragDTO {

    @NotBlank
    @Size(min = 1, max = 99)
    @Pattern(regexp = "^[a-zA-Z0-9_.,!? ;'#]+$", message = "Keine seltsamen Sonderzeichen erlaubt! Hör auf uns zu hacken du Doofi!")
    private String text;

    @NotBlank
    @Pattern(regexp = "^[a-zA-Z0-9_.,!? ;'#]+$", message = "Keine seltsamen Sonderzeichen erlaubt!")
    private String title;

    public BeitragDTO(@Size(min = 1, max = 99) String text, String title) {
        this.text = text;
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
