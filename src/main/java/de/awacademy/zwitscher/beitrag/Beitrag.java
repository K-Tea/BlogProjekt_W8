package de.awacademy.zwitscher.beitrag;

import de.awacademy.zwitscher.comment.Comment;
import de.awacademy.zwitscher.user.User;

import javax.persistence.*;
import java.time.Instant;
import java.util.LinkedList;
import java.util.List;

@Entity
public class Beitrag {

    @Id
    @GeneratedValue
    private long id;

    @ManyToOne
    private User user;

    private String text;
    private String title;

    private Instant postedAt;

    @OneToMany
    private List<Comment> commentList = new LinkedList<>();

    public List<Comment> getCommentList() {
        return commentList;
    }


    public Beitrag(User user, String text, Instant postedAt, String title) {
        this.user = user;
        this.text = text;
        this.postedAt = postedAt;
        this.title = title;

    }

    public Beitrag() {
    }

    public User getUser() {
        return user;
    }

    public String getText() {
        return text;
    }

    public Instant getPostedAt() {
        return postedAt;
    }

    public String getTitle() {
        return title;
    }

    public long getId() {
        return id;
    }
}
