package de.awacademy.zwitscher.useradmin;

import de.awacademy.zwitscher.user.RegistrationDTO;
import de.awacademy.zwitscher.user.User;
import de.awacademy.zwitscher.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class UserAdminController {

    private UserRepository userRepository;

    @Autowired
    public UserAdminController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("/useradmin")
    public String userlist(Model model, @ModelAttribute("sessionUser") User sessionUser) {

        model.addAttribute("userlist",userRepository.findAll());

        return "useradmin";
    }

    @PostMapping("/useradmin/isAdmin/{id}")
    public String makeAdmin(@PathVariable long id, @ModelAttribute("sessionUser") User sessionUser){
        User user = userRepository.findById(id).get();
        user.setAdmin(true);
        userRepository.save(user);
        return "redirect:/useradmin";
    }
}
